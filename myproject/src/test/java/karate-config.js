function fn() {    
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
    creditsimulationURL: 'http://localhost:8080/'
  }
  if (env == 'dev') {
    
  } else if (env == 'e2e') {
    
  }

  karate.configure('retry', { count: 10, interval: 5000 });
  return config;
}