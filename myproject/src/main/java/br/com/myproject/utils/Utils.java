package br.com.myproject.utils;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;


public class Utils {

    public static long getCurrentTimestamp() {
        return new Timestamp((System.currentTimeMillis() / 1000)).getTime();
    }

    public static long getCurrentTimestampPlusMinutes(final int min) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, min);
        return new Timestamp(c.getTimeInMillis() / 1000).getTime();
    }

    public static String getDateNowFormat(final String format) {
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static String getAddMinutesToDate(final String dateTime, final String format, final int minutes) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, minutes);
        return sdf.format(cal.getTime()); 
    }

    public static String formatDateTime(final String dateTime, final String format) throws ParseException {
        final SimpleDateFormat formatt = new SimpleDateFormat("yyyy-MM-dd");
        final Date dateFormat = formatt.parse(dateTime);
        final SimpleDateFormat parser = new SimpleDateFormat(format);
        return parser.format(dateFormat);
    }

    public static String getNextHourFormat(final String format, final int hours) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR_OF_DAY, hours);
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(c.getTime());
     }

 
    public static String generateName() {
     
        int i, nrRandomVowel, nrRandomConsonant;

        final String vowel[] = { "a", "e", "i", "o", "u" };
		String nameYou = "", name = "";
		final String consonant[] = {
		    "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p",
		    "q", "r", "s", "t", "w", "x", "y", "z" 
		};
        final Random random = new Random();
        for (i = 0; i <= 8; i++) {
            nrRandomVowel = 0 + random.nextInt(4);
            nrRandomConsonant = 0 + random.nextInt(19);
            nameYou = vowel[nrRandomVowel] + consonant[nrRandomConsonant];
            name = name + "" + nameYou;
        }
            return name;
    }

    private static String calcDigVerif(final String num) {    
 
        Integer firstDigit; 
        Integer secondDigit;    
        int sum = 0; 
        int weight = 10; 
 
        for (int i = 0; i < num.length(); i++)    
                sum += Integer.parseInt(num.substring(i, i + 1)) * weight--;    
 
                if (sum % 11 == 0 | sum % 11 == 1)    
                    firstDigit = Integer.valueOf(0);    
                else   
                    firstDigit = Integer.valueOf(11 - (sum % 11));    
 
                sum = 0;    
                weight = 11; 
 
        for (int i = 0; i < num.length(); i++)    
                sum += Integer.parseInt(num.substring(i, i + 1)) * weight--;    
 
        sum += firstDigit.intValue() * 2;    
        if (sum % 11 == 0 | sum % 11 == 1)    
            secondDigit = Integer.valueOf(0);    
        else   
            secondDigit = Integer.valueOf(11 - (sum % 11));    
 
        return firstDigit.toString() + secondDigit.toString();    
    }

    public static String generateCPF() {   
 
        String initials = "";    
        Integer number;  
 
        for (int i = 0; i < 9; i++) {    
            number = Integer.valueOf((int) (Math.random() * 10));    
            initials += number.toString();    
        }    
        return initials + calcDigVerif(initials);    
    }

    public static boolean validateCPF(final String cpf) {    
        if (cpf.length() != 11)    
            return false;    
 
        final String numDig = cpf.substring(0, 9);    
        return calcDigVerif(numDig).equals(cpf.substring(9, 11));    
    }

    //////

    

    public static String randomestring() {
      String generatedstring=RandomStringUtils.randomAlphabetic(8);
      return(generatedstring);
     }
  
  
    public static String generateEmail(){
     String email=randomestring()+"@teste.com";
        return email;
    }
  
    public static String randomeNum() {
          String generatedString2 = RandomStringUtils.randomNumeric(4);
          return (generatedString2);
    }
}