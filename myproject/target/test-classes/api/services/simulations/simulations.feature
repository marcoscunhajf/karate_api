Feature: Simulations

  Background:
    * url creditsimulationURL
 
  @positive
  Scenario: Verify post on /api/v1/simulacoes with statusCode 201
    * def name = Java.type('br.com.myproject.utils.Utils').generateName()
    * def cpf = Java.type('br.com.myproject.utils.Utils').generateCPF()
    * def email = Java.type('br.com.myproject.utils.Utils').generateEmail()
    Given path '/api/v1/simulacoes'
    * def body = read('classpath:api/services/simulations/data/simulations.json')
    And request body
    When method post
    Then status 201
    And assert responseType == 'json'
    And match response contains {seguro: '#boolean', id: '#notnull', nome: '#string', id: '#notnull'}

    
  Scenario: Verify post on /api/v1/simulacoes with statusCode 400
    * def data_yaml = read('classpath:api/services/simulations/data/data.yaml')
    * def name = Java.type('br.com.myproject.utils.Utils').generateName()
    * def cpf = Java.type('br.com.myproject.utils.Utils').generateCPF()
    * def email = data_yaml.email.email_blank
    Given path '/api/v1/simulacoes'
    * def body = read('classpath:api/services/simulations/data/simulations.json')
    And request body
    When method post
    Then status 400


#The message and code is different from the model in Swagger. I used the message and the code returned from the simulation in Swagger.
  Scenario: Verify post on /api/v1/simulacoes with statusCode 400
    * def simulation_Positive = call read('classpath:api/services/simulations/simulations.feature@positive')
    * def name = Java.type('br.com.myproject.utils.Utils').generateName()
    * def cpf = simulation_Positive.response.cpf
    * def email = Java.type('br.com.myproject.utils.Utils').generateEmail()
    Given path '/api/v1/simulacoes'
    * def body = read('classpath:api/services/simulations/data/simulations.json')
    And request body
    When method post
    Then status 400
    And match response contains {mensagem: 'CPF duplicado'}


  Scenario: Verify get on /api/v1/simulacoes/ with statusCode 200
    Given path '/api/v1/simulacoes/'
    And request {}
    When method get
    Then status 200
    And assert responseType == 'json'
    And match response[0] contains {seguro: '#boolean', nome: '#string', id: '#notnull'}


  Scenario: Verify get on /api/v1/simulacoes/cpf with statusCode 200
    * def simulation_Positive = call read('classpath:api/services/simulations/simulations.feature@positive')
    * def cpf = simulation_Positive.response.cpf
    Given path '/api/v1/simulacoes/' + cpf
    And request {}
    When method get
    Then status 200
    And assert responseType == 'json'
    And match response contains {seguro: '#boolean', id: '#notnull', nome: '#string'}


#The message is different from the model in Swagger. I used the message returned from the simulation in Swagger.
  Scenario: Verify get on /api/v1/simulacoes/cpf with statusCode 404
    * def data_yaml = read('classpath:api/services/simulations/data/data.yaml')
    * def cpf = data_yaml.cpf.cpf_invalid
    Given path '/api/v1/simulacoes/' + cpf
    And request {}
    When method get
    Then status 404
    And assert responseType == 'json'
    And match response contains {mensagem: 'CPF 99999999999 não encontrado'}


  Scenario: Verify put on /api/v1/simulacoes/cpf with statusCode 200
    * def simulation_Positive = call read('classpath:api/services/simulations/simulations.feature@positive')
    * def name = Java.type('br.com.myproject.utils.Utils').generateName()
    * def cpf = simulation_Positive.response.cpf
    * def email = simulation_Positive.response.email
    Given path '/api/v1/simulacoes/' + cpf
    * def body = read('classpath:api/services/simulations/data/simulations.json')
    And request body
    When method put
    Then status 200
    And assert responseType == 'json'
    And match response contains {seguro: '#boolean', id: '#notnull', nome: '#string'}


  Scenario Outline: Verify put on /api/v1/simulacoes/cpf with statusCode 404
    * def simulation_Positive = call read('classpath:api/services/simulations/simulations.feature@positive')
    * def data_yaml = read('classpath:api/services/simulations/data/data.yaml')
    * def name = simulation_Positive.response.name
    * def cpf = data_yaml.cpf.cpf_invalid
    * def email = simulation_Positive.response.email
    Given path '/api/v1/simulacoes/' + cpf
    * def body = read('classpath:api/services/simulations/data/simulations.json')
    And request body
    When method put
    Then status 404
    And assert responseType == 'json'
    And match response == <expectedResponse>

    Examples:
      | expectedResponse                             |
      | {mensagem: 'CPF 99999999999 não encontrado'} |

  
  Scenario: Verify delete on /api/v1/simulacoes/id with statusCode 200
    * def simulation_Positive = call read('classpath:api/services/simulations/simulations.feature@positive')
    * def id = simulation_Positive.response.id
    Given path '/api/v1/simulacoes/' + id
    And request {}
    When method delete
    Then status 200


#The test was to return 404, because I am passing an id that has no simulation. 200 is returning.
  Scenario: Verify delete on /api/v1/simulacoes/id with statusCode 404
    * def data_yaml = read('classpath:api/services/simulations/data/data.yaml')
    * def id = data_yaml.id.id_invalid
    Given path '/api/v1/simulacoes/' + id
    And request {}
    When method delete
    Then status 404