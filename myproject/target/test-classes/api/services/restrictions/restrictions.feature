Feature: Restrictions

  Background:
    * def data_yaml = read('classpath:api/services/restrictions/data/data.yaml')
    * url creditsimulationURL

  Scenario Outline: Verify get on /api/v1/restricoes/cpf with <status_code>
    * def cpf = data_yaml.cpf.<cpf>
    Given path '/api/v1/restricoes/' + cpf
    When method get
    Then status <status_code>
    And assert responseType == 'json'
    And match response contains {mensagem: "#notnull"}
    

    Examples:
      | cpf         | status_code | 
      | cpf_first   | 200         | 
      | cpf_second  | 200         |
      | cpf_third   | 200         |
      | cpf_fourth  | 200         |
      | cpf_fifth   | 200         |
      | cpf_sixth   | 200         |
      | cpf_seventh | 200         |
      | cpf_eighth  | 200         |
      | cpf_ninth   | 200         |
      | cpf_ten     | 200         |